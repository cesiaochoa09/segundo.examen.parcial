from django.db import models
from Ciudades.models import Ciudades

# Create your models here.
class Estadio(models.Model):
    nombre = models.CharField(max_length=255)
    ciudad = models.ForeignKey(Ciudades, on_delete = models.CASCADE)

    def __str__(self):
        return self.nombre