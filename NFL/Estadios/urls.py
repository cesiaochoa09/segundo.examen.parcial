from django.urls import path
from Estadios import views

app_name = "estadios"

urlpatterns = [
    path('list/', views.List.as_view(), name="list"),
    path("create/", views.Create.as_view(), name="create"),
    path("delete/", views.Delete.as_view(), name="delete"),
    path("detail/", views.Detail.as_view(), name="detail"),
    path("update/", views.Update.as_view(), name="update"),
]