from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Estadio
from .forms import EstadiosForm, UpdateEstadiosForm

# Create your views here.
class Create(generic.CreateView):
    template_name = "estadios/create.html"
    model = Estadio
    form_class = EstadiosForm
    success_url = reverse_lazy('estadios:list')

class Detail(generic.ListView):
    template_name = "estadios/list.html"
    model = Estadio

class List(generic.View):
    template_name = "estadios/detail.html"
    context = {}

    def get(self, request, *args, **kwargs):
        estadios = Estadio.objects.all()
        self.context = {
            "estadios": estadios
        }
        return render(request, self.template_name, self.context)

class Update(generic.CreateView):
    template_name = "estadios/update.html"
    model = Estadio
    form_class = UpdateEstadiosForm
    success_url = reverse_lazy('estadios:list')


class Delete(generic.DeleteView):
    template_name = "estadios/delete.html"
    model = Estadio
    success_url = reverse_lazy('estadios:list')
