from django import forms
from Estadios.models import Estadio

class EstadiosForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={
                    "type": "text",
                        "placeholder": "Ingrese el nombre"
                }
            ),
            "ciudad": forms.Select(attrs={
                    "type": "Select"
                }
            )
        }


class UpdateEstadiosForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={
                    "type": "text",
                        "placeholder": "Ingrese el nombre"
                }
            ),
            "ciudad": forms.Select(attrs={
                    "type": "Select"
                }
            )
        }
