from django import forms
from Ciudades.models import Ciudades

class CiudadesForm(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={
                "type": "text",
                "placeholder": "Ingrese el nombre"
            })
        }

class UpdateCiudadesForm(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={
                "type": "text",
                "placeholder": "Ingrese el nombre"
            })
        }