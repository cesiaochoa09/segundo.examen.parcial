from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Ciudades
from .forms import CiudadesForm, UpdateCiudadesForm

# Create your views here.
class Create(generic.CreateView):
    template_name = "ciudades/create.html"
    model = Ciudades
    form_class = CiudadesForm
    success_url = reverse_lazy('ciudades:list')

class Detail(generic.ListView):
    template_name = "ciudades/list.html"
    model = Ciudades #queryset = GrantGoal.objects.filter(status=True)

class List(generic.View):
    template_name = "ciudades/detail.html"
    context = {}

    def get(self, request, *args, **kwargs):
        ciudades = Ciudades.objects.all()
        self.context = {
            "ciudades": ciudades
        }
        return render(request, self.template_name, self.context)

class Update(generic.CreateView):
    template_name = "ciudades/update.html"
    model = Ciudades
    form_class = UpdateCiudadesForm
    success_url = reverse_lazy('ciudades:list')


class Delete(generic.DeleteView):
    template_name = "ciudades/delete.html"
    model = Ciudades
    success_url = reverse_lazy('ciudades:list')
