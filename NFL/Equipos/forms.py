from django import forms
from Equipos.models import Equipos

class EquiposForm(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={
                    "type": "text",
                        "placeholder": "Ingrese el nombre"
                }
            ),
            "ciudad": forms.Select(attrs={
                    "type": "Select"
                }
            ),
            "estadio": forms.Select(attrs={
                "type": "Select"
                }
            )
        }

class UpdateEquiposForm(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={
                    "type": "text",
                        "placeholder": "Ingrese el nombre"
                }
            ),
            "ciudad": forms.Select(attrs={
                    "type": "Select"
                }
            ),
            "estadio": forms.Select(attrs={
                "type": "Select"
                }
            )
        }