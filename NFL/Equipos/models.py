from django.db import models
from Ciudades.models import Ciudades
from Estadios.models import Estadio

# Create your models here.
class Equipos(models.Model):
    nombre = models.CharField(max_length=255)
    ciudad = models.ForeignKey(Ciudades, on_delete = models.CASCADE)
    estadio = models.ForeignKey(Estadio, on_delete = models.CASCADE)

    def __str__(self):
        return self.nombre