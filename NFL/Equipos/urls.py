from django.urls import path
from Equipos import views

app_name = "equipos"

urlpatterns = [
    path('list/', views.List.as_view(), name="list"),
    path("create/", views.Create.as_view(), name="create"),
    path("delete/", views.Delete.as_view(), name="delete"),
    path("detail/", views.Detail.as_view(), name="detail"),
    path("update/", views.Update.as_view(), name="update"),
]