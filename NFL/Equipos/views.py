from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Equipos
from .forms import EquiposForm, UpdateEquiposForm

# Create your views here.
class Create(generic.CreateView):
    template_name = "equipos/create.html"
    model = Equipos
    form_class = EquiposForm
    success_url = reverse_lazy('equipos:list')

class Detail(generic.ListView):
    template_name = "equipos/list.html"
    model = Equipos #queryset = GrantGoal.objects.filter(status=True)

class List(generic.View):
    template_name = "equipos/detail.html"
    context = {}

    def get(self, request, *args, **kwargs):
        equipos = Equipos.objects.all()
        self.context = {
            "equipos": equipos
        }
        return render(request, self.template_name, self.context)

class Update(generic.CreateView):
    template_name = "equipos/update.html"
    model = Equipos
    form_class = UpdateEquiposForm
    success_url = reverse_lazy('equipos:list')


class Delete(generic.DeleteView):
    template_name = "equipos/delete.html"
    model = Equipos
    success_url = reverse_lazy('equipos:list')
